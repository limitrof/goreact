import React, {Component} from "react";
import PropTypes from 'prop-types';
import GoogleMapReact from 'google-map-react';
import './GMap.css';

class GMap extends Component {
    static propTypes = {
        // center: PropTypes.array, // @controllable
        // zoom: PropTypes.number, // @controllable
        //hoverKey: PropTypes.string, // @controllable
        // clickKey: PropTypes.string, // @controllable
        onCenterChange: PropTypes.func,
        // onZoomChange: PropTypes.func, // @controllable generated fn
        // onHoverKeyChange: PropTypes.func, // @controllable generated fn

        // greatPlaces: PropTypes.array
    };

    static defaultProps = {
        center: {lat: 53.89, lng: 27.56},
        zoom: 7/*,
        greatPlaces: [
            {id: 'Minsk', lat: 53.9, lng: 27.5666},
            {id: 'Homyel', lat: 52.43, lng: 31},
            {id: 'Mahilyow', lat: 53.8985, lng: 30.3247},
            {id: 'Vitsyebsk', lat: 55.1887, lng: 30.1853},
            {id: 'Hrodna', lat: 55.1887, lng: 30.1853},
            {id: 'Brest', lat: 52.1, lng: 23.7}
        ]*/
        /*
Babruysk	53.1266	29.1928
Baranavichy	53.1368	26.0134
Horad Barysaw	54.226	28.4922
Orsha	54.5153	30.4215
Pinsk	52.1279	26.0941
Mazyr	52.046	29.2722
Maladzyechna	54.3188	26.8653
Lida	53.8885	25.2846
Polatsk	55.4894	28.786

        *
        * */
    };
    constructor(props) {
        super(props);
    }

    _onChildClick = (key, childProps) => {
        this.props.onCenterChange(childProps.text);
    }

    render() {
        //const places = [];
        const places = this.props.greatPlaces
            .map((place, index) => {
                const {id, ...coords} = place;
                return (
                    <ForecastPlaceComponent
                        key={index}
                        {...coords}
                        text={id}
                    />
                );
            });
        return (
            <GoogleMapReact
                defaultCenter={this.props.center}
                defaultZoom={this.props.zoom}
                onChildClick={this._onChildClick}
            >
                {places}
            </GoogleMapReact>
        );
    }
}
const ForecastPlaceComponent = ({ text }) => (
    <div style={{
        color: 'white',
        background: 'grey',
        padding: '15px 10px',
        display: 'inline-flex',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: '100%',
        transform: 'translate(-50%, -50%)'
    }}>
        {text}
    </div>
);
export default GMap;
