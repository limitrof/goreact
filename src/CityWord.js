import React, {Component} from "react";
import Search from "react-search/lib/Search";
import {Map} from "react-lodash";
import GMap from "./GMap";
import './cityWord.css';

class CityWorld extends Component {

    constructor(props) {
        super(props)
        this.state = {
            repos: [],
            history: [],
            currentForecast: null,
            greatPlaces: [] // selected by country (default Belarus) or get all (!!!memory)
        }
        this.getHistory();
        this.getCityByCounty();
        this.getForecast = this.getForecast.bind(this);
    }

    HiItems(items) {
        if (items[items.length - 1] && items[items.length - 1].value) {
            this.getForecast(items[items.length - 1].value);
        }
    }

    getHistory() {
        let url = `http://localhost/log`;
        fetch(url).then((response) => {
            return response.json();
        }).then((results) => {
            if (results !== undefined) {
                this.setState({history: results});
            }
        });
    }

    getCityByCounty() {
        let url = `http://localhost/country?country=Belarus`;
        fetch(url).then((response) => {
            return response.json();
        }).then((results) => {
            if (results !== undefined) {
                let items = results.map((res) => {
                    return {id: res.name, lat: res.lat, lng: res.lng}
                });
                this.setState({greatPlaces: items});
            }
        });
    }

    getHistory() {
        let url = `http://localhost/log`;
        fetch(url).then((response) => {
            return response.json();
        }).then((results) => {
            if (results !== undefined) {
                this.setState({history: results});
            }
        });
    }

    getItemsAsync(searchValue, cb) {
        let url = `http://localhost/city?city=${searchValue}`;
        fetch(url).then((response) => {
            return response.json();
        }).then((results) => {
            if (results !== undefined) {
                let items = results.map((res, i) => {
                    return {id: res.id, value: res.name}
                });
                this.setState({repos: items});
                cb(searchValue);
            }
        });
    }

    getForecast(city) {
        if (city) {
            let url = `http://localhost?city=${city}`;
            fetch(url).then((response) => {
                return response.json();
            }).then((results) => {
                if (results !== undefined) {
                    this.setState(
                        {currentForecast: "Прогноз для " + city + ": температура C" + results.main.temp}
                    );
                    this.getHistory();
                }
            });
        }

    }

    render() {
        return (
            <div>
                <Search
                    items={this.state.repos}
                    getItemsAsync={this.getItemsAsync.bind(this)}
                    multiple={true}
                    // placeholder={"введите город для получения прогноза"}
                    onItemsChanged={this.HiItems.bind(this)}/>
                <code>NB: поиск поддерживет опечатки до уровня 2-х перестановок, но база содерит только 15
                    городов РБ Minsk, Homyel', Mahilyow, Vitsyebsk, Hrodna, Brest, Babruysk, Baranavichy,
                    Horad Barysaw (БЛИН!), Orsha, Pinsk, Mazyr, Maladzyechna, Lida, Polatsk
                </code>
                <div>{this.state.currentForecast}</div>
                <hr/>
                <div style={{height: '100vh', width: '100%'}}>
                    <GMap onCenterChange={this.getForecast} greatPlaces={this.state.greatPlaces}/>
                </div>
                <hr/>
                <h3>History:</h3>
                <ul>
                    <Map collection={this.state.history} iteratee={(i, index) => <li key={index}>{i}</li>}/>
                </ul>
            </div>
        )
    }
}

export default CityWorld;
